# Gitlab CI Templates

This repository hosts templates for different needs in Gitlab CI pipelines.

There can be different types of templates, notably job templates and CI
templates.

- Templates that define simple jobs should be put in the `jobs` directory.
- Templates that define actual pipelines stages and variables should be put in
  the `pipelines` directory.